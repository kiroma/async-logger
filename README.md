# Async Logger
This is a rather small project, entirely encapsulated in a single header file, asynclogger.hpp.
This logger is capable of asynchronously streaming any string given to any `std::ostream` given through a reference.
## Usage
You can either use the logger directly by including the `asynclogger.hpp` file in your project and creating an `async_logger` object, or by including `asyncglogger.hpp` and linking against `libasyncglogger`, which provides a global object in the project.
After that, you can simply call the `async_logger::log(const std::string message, std::ostream& = std::cout)` function.
## Caveats
- Because the logger uses references to `std::ostream`, passing a locally defined stream might result in UB.
- There is no exception handling as of yet.
## License
Licensed under MIT License, look at the LICENSE file for more information.
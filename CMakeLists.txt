cmake_minimum_required (VERSION 3.0.2)
project(async-logger CXX)

option(BUILD_TESTING "Should Tests be built?" OFF)
include(CTest)

if(BUILD_TESTING)
	enable_testing()
	add_executable(testlogger
	main.cpp)
	target_link_libraries(testlogger asyncglogger)
	add_test(SanityTest testlogger)
endif(BUILD_TESTING)

add_library(asyncglogger
asynclogger.cpp)

find_package(Threads)

target_link_libraries(asyncglogger PUBLIC Threads::Threads)

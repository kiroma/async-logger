#pragma once
#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>
#include <atomic>
#include <string>
#include <iostream>
#include <utility>

class async_logger
{
	typedef std::queue<std::pair<std::ostream&, std::string>> msg_t;
	msg_t messages{};
	std::mutex readlock;
	std::condition_variable cv;
	std::atomic<bool> eom{false};
	static void loggerThread(msg_t &messages, std::mutex &readlock, std::condition_variable &cv, std::atomic<bool> &eom)
	{
		while(true)
		{
			std::unique_lock<std::mutex> lk(readlock);
			cv.wait(lk, [&messages, &eom]() -> bool{
				return !messages.empty() || eom.load(std::memory_order_relaxed);
			});
			if(messages.empty())
			{
				break;
			}
			std::ostream &ofs = messages.front().first;
			const std::string message = std::move(messages.front().second);
			messages.pop();
			lk.unlock();
			ofs << message << std::endl;
		}
	}
	std::thread loggert;
public:
	void log(const std::string message, std::ostream &ofs = std::cout)
	{
		readlock.lock();
		messages.emplace(ofs, message);
		readlock.unlock();
		cv.notify_one();
	}
	async_logger() : loggert(loggerThread, std::ref(messages), std::ref(readlock), std::ref(cv), std::ref(eom)) {}
	~async_logger()
	{
		eom = true;
		cv.notify_one();
		loggert.join();
	}
};
